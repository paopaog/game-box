import store from "@/store"
let requestTask = null;
let hasLoginModal = false; // false 尚未有打开的登录对话框； true 已有打开的登录对话框
const noTipArr = ['pwd', '请求成功', '验证码', '失效'];
const noLoginArr = ['aa']

function HTTP(obj, config) {
	let defaultConfig = {
		isRes: true,
		loading: false
	}
	config = {
		...defaultConfig,
		...config
	}

	// 如果需要显示loading,mask防止点击穿透
	config.loading && uni.showLoading({
		title: '加载中',
		mask: true
	});

	return new Promise((resolve, reject) => {
		let options = {
			url: "",
			method: "GET",
			timeout: 10000,
			data: {},
			dataType: "json",
			header: {
				"content-type": "application/json", // 'application/x-www-form-urlencoded'
				"X-requested-With": "XMLHttpRequest",
				"XX-Device-Type": "mobile",
				"boxtoken": store.state.userInfo.token || ""
			},
			success: (res) => {
				uni.hideLoading();
				// 状态码为200
				if (res.statusCode == 200) {
					let data = res.data;

					//返回 { code:10000,msg:"消息",data:[] }
					if (config.isRes) {
						// data.rTask = requestTask
						resolve(data)
					}

					//自动校验用户是否登录过期
					if (res.data.status == -1) {
						const arr = noLoginArr.map(i => options.url.indexOf(i) >= 0).filter(Boolean);
						if (arr.length > 0) return
						if (hasLoginModal) return

						hasLoginModal = true
						setTimeout(() => {
							hasLoginModal = false
						}, 1000)
						store.commit('SET_LOGOUT')
						store.dispatch('reLogin');

						// uni.showModal({
						// 	title: '提示',
						// 	content: '当前未登录，是否去登陆？',
						// 	success: function(res) {
						// 		if (res.confirm) {
						// 			store.commit('SET_LOGOUT')
						// 			store.dispatch('reLogin');
						// 		} else if (res.cancel) {
						// 			uni.navigateTo({
						// 				url: '/pages/main/index/index'
						// 			})
						// 		}
						// 		hasLoginModal = false
						// 	}
						// });
					}

					if (data.status == 0 && data.msg) {
						uni.showToast({
							title: data.msg,
							icon: "none",
							duration: 2000
						})
					}

				} else {
					uni.showToast({
						title: "HTTP:状态码异常",
						icon: "none",
					})
					setTimeout(() => {
						uni.hideLoading();
					}, 1300)
					resolve({
						status: 0,
						'msg': 'HTTP:状态码异常！'
					});
				}
			},
			fail: (err) => {
				uni.hideLoading();
				uni.showToast({
					title: "网络异常，请稍后再试!",
					icon: "none",
				})
				reject("网络异常，请稍后再试!");
			},
			complete: () => {
				requestTask = null;
			}
		}

		options = {
			...options,
			...obj
		};

		const OPENID = uni.getStorageSync("openId");
		if (OPENID) options["header"]["openId"] = OPENID;

		if (options.url && options.method) {
			requestTask = uni.request(options);
		} else {
			uni.showToast({
				title: 'HTTP：缺失参数',
				icon: "none",
				duration: 2000
			})
		}
	})

}

export default {
	GET(url, data = {}, config) {
		return HTTP({
			url,
			data,
			method: "GET"
		}, config);
	},
	POST(url, data = {}, config) {
		return HTTP({
			url,
			data,
			method: "POST"
		}, config);
	},

	POSTformdata(url, data = {}, config) {
		return HTTP({
			url,
			data,
			method: "POST"
		}, config);
	}
}