import http from "./http.js";
import config from "@/config/index.config.js";
import channel from "@/static/channel/channel.js";

if (!config.domain) {
	config.domain = channel.domain;
}

//////////////////////////////////////// 通用的 ////////////////////////////////////////////
export const ajax = (url, data) => http.POST(config.domain + url, data);