import Vue from 'vue'
import App from './App'
import store from './store'
// #ifndef VUE3

import uView from '@/uni_modules/uview-ui'
import $apis from './apis/api.js'
import $mRouter from './common/router.js'
Vue.use(uView)
import './uni.promisify.adaptor'
Vue.config.productionTip = false
App.mpType = 'app'
Vue.prototype.$mRouter = $mRouter;
Vue.prototype.$apis = $apis;
Vue.prototype.$store = store;
const app = new Vue({
	...App
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif

var mixin = {
	methods: {
		toBackPage: function() {
			this.$mRouter.back(1)
		},
		simpleWarn(val) {
			uni.showToast({
				title: val,
				icon: 'none'
			});
		}
	}
}
Vue.mixin(mixin)