import Vue from 'vue'
import Vuex from 'vuex'


import {
	getWeather
} from '@/apis/index.js'

Vue.use(Vuex)

const TOKEN = uni.getStorageSync("token") || "";
const WSTOKEN = uni.getStorageSync("wstoken") || "";
const OPENID = uni.getStorageSync("openid") || "";
const USER_INFO = uni.getStorageSync("userInfo") || {};

const store = new Vuex.Store({
	state: {
		jobTypeToggle: false,
		// 是否強制登录
		forcedLogin: false,
		// 前端token
		token: TOKEN,
		wstoken: WSTOKEN,
		// 用户openid
		openId: OPENID,
		// 用户信息 头像 昵称
		userInfo: USER_INFO
		
	},
	getters: {
		getSelectJobType(state) {
			console.log(state.jobTypeToggle);
			return state.jobTypeToggle
		},

		getUserInfo(state) {
			return state.userInfo
		},

		// 用户是否登录
		hasLogin: state => {
			if (state.token) {
				return true;
			} else {
				return false
			}
		},

		// 是否设置了角色
		isSetedRole(state) {
			if (!state.userRole) {
				return false
			}
			return true
		},

		getRole(state) {
			return state.userRole
		},

		getLocation(state) {
			return state.location
		},

		returnWeather(state) {
			return state.weather
		},

		getIsArea(state) {
			return state.isArea
		}
	},
	mutations: {
		SET_TOKEN(state, token) {
			state.token = token;
			uni.setStorageSync("token", token);
			uni.removeStorageSync('invite_uid');
		},
		SET_WSTOKEN(state, wstoken) {
			state.wstoken = wstoken;
			uni.setStorageSync("wstoken", wstoken);
			uni.removeStorageSync('invite_uid');
		},
		SET_OPENID(state, openId) {
			state.openId = openId;
			uni.setStorageSync("openId", openId);
		},
		SET_USERINFO(state, userInfo) {
			state.userInfo = userInfo;
			uni.setStorageSync("userInfo", userInfo);
		},
		SET_ROLE(state, role) {
			const options = ['boss', 'worker']
			if (options.indexOf(role) == -1) {
				throw '角色可选项为' + JSON.stringify(options)
			}
			state.userRole = role;
			uni.setStorageSync("user_role", state.userRole);
		},
		SET_LOGOUT(state) {
			state.token = "";
			state.wstoken = "";
			state.userInfo = {};
			uni.setStorageSync("token", "");
			uni.setStorageSync("wstoken", "");
			uni.setStorageSync("userInfo", "");
		},
		SET_WEATHER(state, weather) {
			state.weather = weather;
			uni.setStorageSync("weather", JSON.stringify(weather));
		},
		SET_LOCATION(state, {
			location,
			isChangeCity = false
		}) {
			let keyArr = ['province', 'city', 'area', 'lon', 'lat']
			uni.setStorageSync("user_location", JSON.stringify(location))
			// if(Object.keys(location).some(i=> keyArr.join(',').indexOf(i) == -1 )){
			// 	console.error('传入的对象参数必须拥有以下key:' + keyArr.join(',') + '您传入的key为:' + Object.keys(location).join(','))
			// 	return
			// }
			state.location = location
			return
			this.dispatch('getWeather', isChangeCity);
		},
		SET_LOCATION_LEVEL(state, type) {
			state.isArea = type
			uni.setStorageSync('is_area', type)
		}
	},
	actions: {
		async getWeather({
			state
		}, isChangeCity = false) {
			const currentTimestamp = new Date().getTime();
			const isNeedGetWeather = () => {
				return !state.weather || (currentTimestamp - state.weather.timestamp) / 1000 / 60 >
					60 || isChangeCity
			}
			if (isNeedGetWeather()) {
				try {
					const res = await getWeather({
						city: state.location.city,
						area: state.location.area
					})
					this.commit('SET_WEATHER', res.data)
				} catch (e) {
					uni.hideToast()
					uni.showToast({
						icon: 'none',
						title: '获取天气失败'
					})
					//TODO handle the exception
				}
			}
		},

		// 登录过期 重新登录
		reLogin({
			commit
		}, info) {
			commit("SET_TOKEN", "");
			uni.redirectTo({
				url: '/pages/login/passwordLogin',
				fail(e) {
					console.log(e);
				}
			})
		}
	}
})

export default store