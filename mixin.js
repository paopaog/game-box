var mixin = {
	data: function() {
		return {}
	},
	created: function() {},
	methods: {

		/**
		 * @param {Object} data
		 */
		wechatAppPay(data, callback, payName) {
			const obj = {
				MobilePayAlipay: 'alipay',
				MobilePayWechat: 'wxpay'
			}
			uni.requestPayment({
				provider: obj[payName],
				orderInfo: data,
				success: (e) => {
					if (e.errMsg.indexOf('ok') >= 0) {
						callback && callback()
					} else if (e.errMsg.indexOf('fail') >= 0) {
						this.simpleWarn('支付失败，请稍后重试')
					}
					// callback && callback()
					// console.log(e);
				},
				fail: (err) => {
					if (err.errMsg.includes('User canceled')) {
						this.simpleWarn('您已取消支付')
					} else {
						this.simpleWarn('支付失败，请稍后重试')
					}
				}
			})
		},

		getDefaultAvatarBySex(sex = 0) {
			const arr = [{
					sex: 0,
					name: '女',
					img: '/static/img/work_man/work_female.png'
				},
				{
					sex: 1,
					name: '男',
					img: '/static/img/work_man/work_male.png'
				}
			]
			return arr.filter(i => i.sex == sex)[0].img
		},

		isWeixin() {
			// #ifdef H5
			var ua = navigator.userAgent.toLowerCase();
			if (ua.indexOf("micromessenger") > -1) {
				return true
			}
			return false
			// #endif

			// #ifdef APP-PLUS
			return false
			// #endif
		},

		mixGetDate({
			type,
			add = 3,
			minus = 0
		}) {
			const date = new Date();
			let year = date.getFullYear();
			let month = date.getMonth() + 1;
			let day = date.getDate();

			if (type === 'start') {
				year = year - minus;
			} else if (type === 'end') {
				year = year + add;
			}
			month = month > 9 ? month : '0' + month;;
			day = day > 9 ? day : '0' + day;
			return `${year}-${month}-${day}`;
		},


		/**
		 * @param { String } e
		 */
		copyContact(e) {
			let result = false
			// #ifdef APP-PLUS || MP-WEIXIN
			uni.setClipboardData({
				data: e,
				success() {
					result = true
					uni.showToast({
						title: '复制成功',
						icon: 'none'
					})
				},
				fail() {
					result = false
					uni.showToast({
						title: '复制失败，请稍后再试',
						icon: 'none'
					})
				}
			});
			// #endif

			// #ifdef H5
			result = this.h5Copy(e);
			uni.showToast({
				title: '复制成功',
				icon: 'none'
			})
			// #endif
			return result;
		},

		/**
		 * 接受一个数组，和object的key，将数组中key对应的值加到image上
		 * 方便组件使用数据
		 * @param {Array[Object]} arr 指定数组
		 * @param {String} 		  key 被映射值的key
		 */
		formatImgPath(arr, key = "image") {
			return arr.map(item => {
				item.image = this.$mConfig.baseUrl + '/upload/' + item[key]
				return item
			})
		},

		formatSingleImg(path) {
			return this.$mConfig.baseUrl + '/upload/' + path
		},

		/**
		 * 接受一个数组，和object的key，将数组中的key对应的值加到text上
		 * 方便组件使用数据
		 * @param {Array[Object]} arr 指定数组
		 * @param {String}        key 被映射值的key
		 */
		formatTextName(arr, key) {
			return arr.map(item => {
				item.text = item[key]
				return item
			})
		},

		/**
		 * 上传图片
		 */
		mixUploadImg() {
			return new Promise((resolve, reject) => {
				uni.chooseImage({
					sizeType: ['compressed'],
					success: chooseImageRes => {
						const tempFilePaths = chooseImageRes.tempFilePaths;
						const uploadTask = uni.uploadFile({
							url: this.$mConfig.uploadImg, //
							filePath: tempFilePaths[0],
							name: 'file',
							header: {
								'XX-Device-Type': 'mobile',
								'XX-Token': this.$store.state.token || ''
							},
							formData: {},
							success: res => {
								var data = JSON.parse(res.data)
								if (data.code == 1) {
									resolve({
										image: this.formatSingleImg(data
											.data.filepath),
										imageForServer: data.data.filepath
									})
								}
							}
						});

						// this.uploadPercent(uploadTask)
					},
					fail(e) {
						console.log(e);
					}
				});
			})
		},

		uploadPercent(task) {
			task.onProgressUpdate(res => {
				if (res.progress < 100) {
					uni.showToast({
						icon: 'loading',
						title: '进度' + JSON.stringify(res.progress)
					});
				} else {
					uni.hideToast()
				}
			});
		},

		mixUploadResume() {
			this.uploadCommonFn()
		},

		uploadCommonFn() {
			return new Promise((resolve, reject) => {
				uni.chooseFile({
					compressed: true,
					sourceType: ['album', 'camera'],
					success: chooseImageRes => {
						if (chooseImageRes.size > 1000000 * 10) {
							uni.showToast({
								title: '文件过大',
								duration: 1500,
								icon: 'none'
							})
							return
						}
						const tempFilePath = chooseImageRes.tempFilePath;
						uni.showToast({
							title: '视频上传中',
							icon: 'loading'
						});

						const uploadTask = uni.uploadFile({
							url: this.$mConfig.baseUrl +
								'/api.php/portal/Public/upload_img', //
							filePath: tempFilePath,
							name: 'file',
							header: {
								'XX-Device-Type': 'mobile',
								'XX-Token': this.$store.state.token || ''
							},
							formData: {
								file: 'file',
								// filetype: 'video'
							},
							success: res => {
								var data = JSON.parse(res.data)
								if (data.code == 1) {
									uni.hideLoading()
									resolve({
										image: this.formatSingleImg(data
											.data.filepath),
										imageForServer: data.data.filepath
									})
								}
							},
							fail: () => {
								uni.hideLoading()
							}
						});

						// this.uploadPercent(uploadTask)
					},
					fail: (e) => {
						reject(false)
						console.log('fail' + JSON.stringify(e));
					},
					complete: (e) => {
						console.log('complete' + JSON.stringify(e));
					}
				});
			})
		},

		/**
		 * 上传视频
		 */
		mixUploadVideo() {
			return new Promise((resolve, reject) => {
				uni.chooseVideo({
					compressed: true,
					sourceType: ['album', 'camera'],
					success: chooseImageRes => {
						if (chooseImageRes.size > 1000000 * 10) {
							uni.showToast({
								title: '文件过大',
								duration: 1500,
								icon: 'none'
							})
							return
						}
						const tempFilePath = chooseImageRes.tempFilePath;
						uni.showToast({
							title: '视频上传中',
							icon: 'loading'
						});

						const uploadTask = uni.uploadFile({
							url: this.$mConfig.baseUrl +
								'/api.php/portal/Public/upload_img', //
							filePath: tempFilePath,
							name: 'file',
							header: {
								'XX-Device-Type': 'mobile',
								'XX-Token': this.$store.state.token || ''
							},
							formData: {
								file: 'video',
								filetype: 'video'
							},
							success: res => {
								var data = JSON.parse(res.data)
								if (data.code == 1) {
									uni.hideLoading()
									resolve({
										image: this.formatSingleImg(data
											.data.filepath),
										imageForServer: data.data.filepath
									})
								}
							},
							fail: () => {
								uni.hideLoading()
							}
						});

						// this.uploadPercent(uploadTask)
					},
					fail: (e) => {
						reject(false)
						console.log('fail' + JSON.stringify(e));
					},
					complete: (e) => {
						console.log('complete' + JSON.stringify(e));
					}
				});
			})
		},

		/**
		 * @param {Object} obj 
		 * 
		 *  isLoad: 		true == 正在加载； false == 未在加载,，
		 *	reqBody: 		请求参数, 
		 * 	reqFunc:        请求方法
		 *  pageData {Object} pageData.page    分页参数 -- 当前页
		 *  pageData {Object} pageData.allPage 分页参数 -- 分页总数
		 */
		async loadMore({
			// isLoad,
			reqBody,
			reqFunc,
			pageData
		}) {
			return new Promise(async (resolve, reject) => {
				console.log(pageData)
				// 正在加载 或者 当前请求的页数大于总页数 且 已初始化过总页数 停止发送请求
				if (pageData.isLoad || (reqBody.page > pageData.allPage && pageData.allPage != -
					1)) {
					return false;
				}
				pageData.isLoad = true
				let res;
				if (reqFunc) {
					try {
						res = await reqFunc(reqBody)
					} catch (e) {
						//TODO handle the exception
					}
				}
				if (res.code == 1) {
					pageData.isLoad = false
					reqBody.page += 1;
					let data = Array.isArray(res.data.data) ? res.data : res.data.list

					if (pageData.allPage == -1) {
						pageData.allPage = data.last_page
					}
					resolve(data)
				} else {
					reject(res)
				}
			})

		},

		/**
		 * @param {Object} obj 
		 * 
		 *  isLoad: 		true == 正在加载； false == 未在加载,，
		 *	reqBody: 		请求参数, 
		 * 	reqFunc:        请求方法
		 *  pageData {Object} pageData.page    分页参数 -- 当前页
		 *  pageData {Object} pageData.allPage 分页参数 -- 分页总数
		 */
		async loadmore({
			url,
			fun,
			pageinfo
		}) {
			return new Promise(async (resolve, reject) => {
				let res;
				if (pageinfo.loading) {
					return false;
				}
				if (pageinfo.totalcount != -1 && (pageinfo.pageindex - 1 >= Math.ceil(pageinfo
						.totalcount / pageinfo.pagesize))) {
					return false;
				}
				pageinfo.loading = true;
				if (fun) {
					try {
						res = await fun(url, pageinfo)
					} catch (e) {
						//TODO handle the exception
					}
				}
				if (res.status == 1) {
					pageinfo.pageindex = pageinfo.pageindex + 1;
					pageinfo.totalcount = res.totalcount;
					pageinfo.loading = false;
					resolve(res)
				} else {
					reject(res)
				}
			})

		},

		/**
		 * 根据经纬度获取详细信息
		 * @param {Object} longitude
		 * @param {Object} latitude
		 */
		mixAnalysisLcoation({
			longitude,
			latitude
		}) {
			const baseUrl = 'https://restapi.amap.com/v3/geocode/regeo?'
			const key = 'key=8505420999b92bf544262e647df10c68'
			const location = `location=${longitude},${latitude}`
			const decodeUrl = `${baseUrl}${key}&${location}`
			return new Promise((resolve, reject) => {
				uni.request({
					method: "GET",
					url: decodeUrl,
					success: (e) => {
						if (e.data.status == 1) {
							resolve(e.data.regeocode)
							// this.formatLocationAndSave(e.data.regeocode)
						}
					},
					fail: (e) => {
						// uni.showModal({
						// content: '解析失败：' + JSON.stringify(e)
						// })
						console.dir(e);
					}
				})
			})
		},
		formatDate(date, fmt) {
			if (typeof date == 'string') {
				date = new Date(date);
			}

			if (!fmt) fmt = "yyyy-MM-dd hh:mm:ss";

			if (!date || date == null) return null;
			var o = {
				'M+': date.getMonth() + 1, // 月份
				'd+': date.getDate(), // 日
				'h+': date.getHours(), // 小时
				'm+': date.getMinutes(), // 分
				's+': date.getSeconds(), // 秒
				'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
				'S': date.getMilliseconds() // 毫秒
			}
			if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1
				.length))
			for (var k in o) {
				if (new RegExp('(' + k + ')').test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[
					k]) : (('00' + o[k]).substr(('' + o[k]).length)))
			}
			return fmt;
		},
		/**
		 * 从树结构数据源中 根据渠道id和类型提取渠道名称
		 * @param {any} promotion_list
		 * @param {any} datarow
		 * @param {any} promotion_id
		 * @param {any} promotion_type
		 */
		get_promotion_title(promotion_list, datarow, promotion_id, promotion_type) {
			if (datarow) {
				promotion_id = datarow["promotion_id"] || 0;
				promotion_type = datarow["promotion_type"] + "";

			}
			var promotion_title = ""
			if (parseInt(promotion_id) > 0) {
				var temp_promotion = promotion_list.plist.find(o => o.id == promotion_id);
				var temp_staff = promotion_list.slist.find(o => o.id == promotion_id);
				if (temp_promotion) {
					promotion_title = "<span style='color:#EB7347;'>" + temp_promotion.label + "</span>";
				} else if (temp_staff) {
					promotion_title = "<span style='color:#00CCFF;'>" + temp_staff.label + "</span>";
				} else {
					promotion_title = "<font color='red'>未知渠道</font>";
				}
			} else {
				promotion_title = "官方直属"
			}
			return promotion_title;
		},
		//强制保留N位小数,自动补0
		todecimal(value, length) {
			var value = Math.round(parseFloat(value) * 10000) / 10000;
			var xsd = value.toString().split(".");
			if (xsd.length == 1) {
				var zero = "";
				for (var i = 1; i <= length; i++) {
					zero = zero + "0";
				}
				value = value.toString() + "." + zero;
				return value;
			}
			if (xsd.length > 1) {
				if (xsd[1].length < length) {
					var zero = xsd[1];
					for (var i = (xsd[1] + "").length; i < length; i++) {

						zero = zero + "0";

					}
					value = xsd[0] + "." + zero;
				}
				return value;
			}
		},
		getDateDiff(dateStr) {
			dateStr = this.formatDate(dateStr);
			dateStr = Date.parse(dateStr.replace(/-/gi, "/"));
			var publishTime = dateStr / 1000,
				d_seconds,
				d_minutes,
				d_hours,
				d_days,
				timeNow = parseInt(new Date().getTime() / 1000),
				d,
				date = new Date(publishTime * 1000),
				Y = date.getFullYear(),
				M = date.getMonth() + 1,
				D = date.getDate(),
				H = date.getHours(),
				m = date.getMinutes(),
				s = date.getSeconds();

			//小于10的在前面补0
			if (M < 10) {
				M = '0' + M;
			}
			if (D < 10) {
				D = '0' + D;
			}
			if (H < 10) {
				H = '0' + H;
			}
			if (m < 10) {
				m = '0' + m;
			}
			if (s < 10) {
				s = '0' + s;
			}
			d = timeNow - publishTime;
			d_days = parseInt(d / 86400);
			d_hours = parseInt(d / 3600);
			d_minutes = parseInt(d / 60);
			d_seconds = parseInt(d);
			if (d_days > 0 && d_days < 30) {
				return d_days + '天前';
			} else if (d_days <= 0 && d_hours > 0) {
				return d_hours + '小时前';
			} else if (d_hours <= 0 && d_minutes > 0) {
				return d_minutes + '分钟前';
			} else if (d_seconds < 60) {
				if (d_seconds <= 0) {
					return '刚刚';
				} else {
					return d_seconds + '秒前';
				}
			} else if (d_days >= 30 && d_days < 365) {
				return parseInt(d_days / 30) + '月前'
			} else if (d_days >= 365) {
				return d_days / 365 + '年前'
			}
		},
		h5Copy(content) {
			if (!document.queryCommandSupported('copy')) {
				// 不支持
				return false
			}

			let textarea = document.createElement("textarea")
			textarea.value = content
			textarea.readOnly = "readOnly"
			document.body.appendChild(textarea)
			textarea.select() // 选择对象
			textarea.setSelectionRange(0, content.length) //核心
			let result = document.execCommand("copy") // 执行浏览器复制命令
			textarea.remove()
			return result

		},

		/**
		 * 发送请求时进行"防抖"，间隔一秒内的操作只执行最后一次。
		 * @param {Function} fn  	需要进行防抖的函数
		 * @param {Object} column   防抖函数所需的参数
		 * @param {Object} index    防抖函数所需的参数
		 */
		requestDebounce(fn, column, index) {
			if (this.clock) {
				clearTimeout(this.clock);
			}
			this.clock = setTimeout(() => {
				if (fn) {
					fn(column, index)
				}
				clearTimeout(this.clock);
			}, 1000);
		},

		/**
		 * @description 返回上一层
		 * 
		 */
		ysBack: function() {
			uni.navigateBack({

			})
		}
	}
}

export default mixin;