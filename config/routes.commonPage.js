export default {
	auth: {
		name: "实名认证",
		path: "auth/auth",
		requiresAuth: true,
	},
	selectCity: {
		name: "选择城市",
		path: "/pages/commonPage/selectCity/selectCity",
		requiresAuth: false,
	},
	multipleRowsFilter: {
		name: "多列选择器",
		path: "/pages/commonPage/multipleColumnFilter/multipleColumnFilter",
		requiresAuth: false,
	},
	search: {
		name: "多列选择器",
		path: "/pages/commonPage/search/search",
		requiresAuth: false,
	},
	authSettings: {
		name: "多列选择器",
		path: "/pages/commonPage/authSettings/authSettings",
		requiresAuth: false,
	},
	tipOff: {
		name: "举报",
		path: "/pages/commonPage/tipOff/tipOff",
		requiresAuth: false,
	},
}