/* 
 * 路由表对象：
 * 该文件挂载在Vue原型中 $mRoutesConfig
 * 作用：调用$mRouter对象的方法 传入以下对应的路由对象，详细见common目录下的router.js
 * 示例：this.$mRouter.push({route:this.$mRoutesConfig.main,query:{a:1}})
 * 注意：所有在pages目录下新建的页面都必须在"路由表"中进行声明，并且在框架的pages.json注册。
 * 
 * 配置参数项说明： 
 * name:可选配置 （路由名称）
 * path:必填配置 （路由地址）
 * requiresAuth:可选配置 （是否权限路由）
 */
import commonRoutes from './routes.commonPage.js'; // 通用路由
import loginRoutes from '../routes/routes.login.js'; // 注册登录相关 路由
import indexRoutes from '../routes/routes.index.js'; // 首页路由
import gameRoutes from '../routes/routes.gamecenter.js'; // 游戏试玩相关 路由
import messageRoutes from '../routes/routes.message.js'; // 消息路由
import myRoutes from '../routes/routes.usercenter.js'; // 我的 路由



const basePath = '/pages/'
// export default
const paths = {
	...commonRoutes,
	...loginRoutes,
	...indexRoutes,
	...gameRoutes,
	...messageRoutes,
	...myRoutes
}

const pathObj = {
	webview: {
		name: "webview容器",
		path: "webview/webview",
	}


}

for (let s in pathObj) {
	pathObj[s].path = basePath + pathObj[s].path
}


export default {
	...pathObj,
	...paths
};